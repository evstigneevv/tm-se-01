package ru.evstigneev.tm.service;

import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectTaskManager {

    private List<Project> projectList = new ArrayList<>();
    private Project currentProject;

    public Project createProject(String projectName) {
        Project project = new Project(projectName, UUID.randomUUID().toString());
        projectList.add(project);
        currentProject = project;
        return project;
    }

    public void showProjectList() {
        if (!projectList.isEmpty()) {
            for (Project project : projectList)
                System.out.println("Project uuid: " + project.getProjectId() + " | Project name: "
                        + project.getName());
        } else {
            System.out.println("There are no projects!");
        }
    }


    public Task createTask(String taskName) {
        return currentProject.addTask(taskName);
    }

    public void showCurrentProjectTasks() {
        currentProject.showTaskList();
    }

    public Project switchProject(String projectName) {
        for (Project project : projectList) {
            if (projectName.equals(project.getName())) {
                currentProject = project;
                System.out.println("current project changed to \"" + currentProject.getName() + "\"");
            }
        }
        return currentProject;
    }

    public void showCurrentProject() {
        if (currentProject == null) {
            System.out.println("Select current project first!");
        } else {
            System.out.println("current project is " + currentProject.getName());
        }
    }

    public boolean deleteProject(String projectName) {
        for (Project project : projectList) {
            if (projectName.equals(project.getName())) {
                System.out.println("Project " + project.getName() + " removed!");
                projectList.remove(project);
                if (currentProject == project) {
                    if (!projectList.isEmpty()) {
                        currentProject = projectList.get(0);
                    } else currentProject = null;
                }
                return true;
            }
        }
        return false;
    }

    public void updateProject(String newProjectName) {
        currentProject.setName(newProjectName);
        System.out.println("Project name updated");
    }

    public void updateTaskName(String taskName) {
        currentProject.updateTaskName(taskName);
        System.out.println("Task name updated");
    }

    public void deleteTask(String taskId) {
        currentProject.showTaskList();
        currentProject.deleteTask(taskId);
        System.out.println("Task deleted");
    }

}
