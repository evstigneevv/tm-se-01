package ru.evstigneev.tm.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Project {

    private String name;
    private String projectId;
    private List<Task> taskList = new ArrayList<>();

    public Project(String name, String projectId) {
        this.name = name;
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void showTaskList() {
        int i = 0;
        while (i < taskList.size()) {
            System.out.println("Task uuid: " + taskList.get(i).getTaskId() + " | Task name: " + taskList.get(i).getName());
            i++;
        }
    }

    public Task addTask(String taskName) {
        Task newTask = new Task(taskName, UUID.randomUUID().toString());
        taskList.add(newTask);
        return newTask;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public void updateTaskName(String taskName) {
        for (Task t : taskList) {
            if (t.getName().equals(taskName)) {
                t.setName(taskName);
            }
        }
    }

    public void deleteTask(String taskName) {
        for (Task t : taskList) {
            if (t.getName().equals(taskName)) {
                taskList.remove(t);
            }
        }
    }

}
